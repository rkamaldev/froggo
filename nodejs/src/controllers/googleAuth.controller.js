/**
 * Created by kamal on 7/5/17.
 */
let authConfig = require('../lib/authConfig');
import {generateCookie, updateUserPofile} from '../models/user.model';
let google = require('googleapis');
let OAuth2 = google.auth.OAuth2;
let request = require('request');

let CALLBACK_URL = process.env.serverHostName;;

export const handleGoogleAuth = (req, res, next) => {
    console.log(authConfig.google.CALLBACK_URL)
    let oauth2Client = new OAuth2(authConfig.google.GOOGLE_APP_ID, authConfig.google.GOOGLE_CLIENT_SECRET, CALLBACK_URL + authConfig.google.CALLBACK_URL);

    let scopes = [
        'https://www.googleapis.com/auth/userinfo.profile'
    ];

    let url = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes
    });

    url = decodeURIComponent(url);
    res.redirect(url);
};

export const handleGoogleAuthCallBack = (req, res, next) => {
    let code = req.query.code;
    let oauth2Client1 = new OAuth2(authConfig.google.GOOGLE_APP_ID,
        authConfig.google.GOOGLE_APP_SECRET,
        CALLBACK_URL + authConfig.google.CALLBACK_URL);

    oauth2Client1.getToken(code, function(err1, tokens) {
        if(err1){
            res.status(500).send('Error when importing your google profile: ' + err1);
        } else {
            request.get({
                url: 'https://www.googleapis.com/plus/v1/people/me',
                headers: {
                    'Authorization': 'OAuth ' + tokens.access_token,
                    'GData-Version': '3.0'
                }
            }, function (err, resp, body) {
                if (!err) {
                    let tempSessionId = generateCookie();

                    let user = JSON.parse(body);
                    let profile = {
                        session: tempSessionId,
                        profileId: 'google_'+user.id,
                        name: user.displayName,
                        familyName: user.name.familyName,
                        givenName: user.name.givenName,
                        photo: user.image.url,
                        lastLogin: new Date().getTime()
                    };

                    console.dir(profile)

                    updateUserPofile(profile)
                        .then(()=>{
                            res.cookie("froggo_user", tempSessionId, {domain   : 'froggo.org', maxAge: 365 * 24 * 60 * 60 * 1000});
                            return res.redirect(CALLBACK_URL + '#success');
                        });
                } else {
                    res.status(500).send('Error when importing your google profile: ' + err);
                }
            });
        }
    })
};