var Joi = require('joi');

module.exports = {
    domain:{
        query: {
            domain: Joi.string().required()
        }
    }
};