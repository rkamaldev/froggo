/**
 * Created by kamal on 7/16/17.
 */
var serverHostName = process.env.serverHostName;
import {
    addManualEntry,
    viewAllNews,
    deleteSelectedNews,
    passwordSetting,
    findAllUsers } from '../models/admin.model'

export const addManualUrl = (req, res) => {
    var submit_url = req.body.url;
    var submit_category = req.body.category;
    console.log(submit_category + ' ' + submit_url)
    addManualEntry(submit_url, submit_category, true)
        .then(data => {
            res.redirect(serverHostName + '/api/froggo/admin');
        })
};

export const viewNews = (req, res) => {
    viewAllNews()
        .then(data => {
            res.render('views/view.html', {domains: data, dasgboardTitle: 'View', serverHostName:process.env.serverHostName});
        })
};

export const deleteNews = (req, res) => {
    deleteSelectedNews(req.params.id, req.params.imageId)
        .then(data => {
            res.redirect(serverHostName + '/api/froggo/admin/view');
        })
};

export const accountSetting = (req, res) => {
    passwordSetting( req.body.current, req.body.pass, req.body.cPass)
        .then(data => {
            res.render('views/account.html', {errorMsg: data, dasgboardTitle: 'Account', serverHostName:process.env.serverHostName});
        })
};

export const getAllUsers = (req, res) => {
    findAllUsers()
        .then((users)=>{
            res.render('views/users.html', {users: users});
        })
};






