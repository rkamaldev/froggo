/**
 * Created by kamal on 7/16/17.
 */
var serverHostName = process.env.serverHostName;
import {findAdminUsersByIds, updateAdminUserProfile, generateCookie1} from '../models/admin.model'
export const adminLogin = (req, res) => {
    if(req.body && req.body.email && req.body.password){
        findAdminUsersByIds({email:req.body.email, password:req.body.password})
            .then(data => {
                if(data) {
                    var tempSessionId = generateCookie1();
                    updateAdminUserProfile(tempSessionId)
                        .then((user)=>{
                            res.cookie("froggo_admin_user", tempSessionId, {domain   : 'froggo.org', maxAge: 900000});
                            res.redirect(serverHostName + '/api/froggo/admin');
                        });
                } else {
                    res.status(401).send('your session might expired')
                }
            }, error => {
                res.status(500).send(error);
            })
    } else {
        res.status(400).send('invalid request')
    }

};

export const adminLogout = (req, res) => {
    res.cookie("froggo_admin_user", null, {domain: 'froggo.org', maxAge: 1});
    res.redirect(serverHostName + '/api/froggo/admin/login');
};

