/**
 * Created by kamal on 7/2/17.
 */

import {getCommentsByDomainFromMongo, getAllDomains} from '../models/public.model'
import {findUsersByIds, findUserBySession} from '../models/user.model'
import {findRepliesByCommentIds} from '../models/reply.model'
import {findVotesByCommentIds} from '../models/vote.model'

export const getCommentsByDomain = (req, res) => {
    if(req.query && req.query.domain){
        if(!req.query.domain.match(/www\.|http:|https:/)){
            req.query.domain = req.query.domain.replace(/\/$/,'');
            getCommentsByDomainFromMongo(decodeURIComponent(req.query.domain).toLocaleLowerCase())
                .then(data => {
                    let result = {
                        comments: data,
                    };

                    result.commentIds = data.map(item => {
                        return item.id
                    });

                    if(result.commentIds.length){

                        let userIds = data.map(item => {
                            return item.userId
                        });

                        var promises = [];
                        promises [0]  = findRepliesByCommentIds(result.commentIds)
                            .then (replies => {
                                result.replies = replies;
                                let repliedUserIds = replies.map(reply => {
                                    return reply.userId;
                                });
                                userIds = userIds.concat(repliedUserIds);
                                return Promise.resolve();
                            })
                            .catch(err=>{
                                return Promise.reject(err);
                            });

                        promises [1]  = findVotesByCommentIds(result.commentIds)
                            .then (votes => {
                                result.votes = votes;
                                let repliedUserIds = votes.map(reply => {
                                    return reply.userId;
                                });
                                userIds = userIds.concat(repliedUserIds);
                                return Promise.resolve();
                            })
                            .catch(err=>{
                                return Promise.reject(err);
                            });

                        if(req.cookies.froggo_user){
                            promises [2] = findUserBySession(req.cookies.froggo_user)
                                .then (user => {
                                    if(user){
                                        delete user.session;
                                    }

                                    result.cUser = user;

                                    return Promise.resolve();
                                })
                                .catch(err=>{
                                    return Promise.reject(+err);
                                });
                        }

                        Promise.all(promises)
                            .then(() => {
                                userIds = userIds.filter( (item, index, inputArray ) => {
                                    return inputArray.indexOf(item) == index;
                                });
                                findUsersByIds(userIds)
                                    .then(users => {
                                        users = users.map(user => {
                                            delete user.session;
                                            return user;
                                        });
                                        result.users = users;
                                        res.status(200).send(result);
                                    })})
                            .catch(error => {console.log('error while executing promise all ' + error)});
                    } else {
                        res.status(200).send(result);
                    }



                }, error => {
                    res.status(500).send(error);
                });
        } else {
            res.status(400).send('invalid url')
        }
    } else {
        res.status(400).send('invalid request')
    }
};

export const getAllDomainsData = (req, res) => {
    getAllDomains()
        .then(data => {
            res.render('index.html', {domains: data, serverHostName:process.env.serverHostName});
        });
};
