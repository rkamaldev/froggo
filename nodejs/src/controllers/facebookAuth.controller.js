/**
 * Created by kamal on 7/5/17.
 */
import passport from '../lib/facebookAuth';
import {generateCookie, updateUserPofile} from '../models/user.model';

var CALLBACK_URL = process.env.serverHostName;

export const handleFacebookAuth = passport.authenticate('facebook', {
    display: 'popup',
    scope: [
        'public_profile'
    ]
});

export const handleAuthCallBack = (req, res, next) => {
    passport.authenticate('facebook', function(err, user, info) {
        if (err) {
            return res.redirect(CALLBACK_URL + '#failed ' + err);
        }

        if (!user) {
            return res.redirect(CALLBACK_URL + '#canceled');
        } else {

            if(user.profile.id){
                let tempSessionId = generateCookie();
                let profile = {
                    session: tempSessionId,
                    profileId: 'facebook_'+user.profile.id,
                    name: user.profile.displayName,
                    familyName: user.profile.name.familyName,
                    givenName: user.profile.name.givenName,
                    photo: user.profile.photos && user.profile.photos[0] ? user.profile.photos[0].value: null,
                    lastLogin: new Date().getTime()
                };

                updateUserPofile(profile)
                    .then(()=>{
                        res.cookie("froggo_user", tempSessionId, {domain   : 'froggo.org', maxAge: 365 * 24 * 60 * 60 * 1000});
                        return res.redirect(CALLBACK_URL + '#success');
                    });

            } else {
                return res.redirect(CALLBACK_URL + '#canceled');
            }
        }
    })(req, res, next);
};

