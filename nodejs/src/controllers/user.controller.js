/**
 * Created by kamal on 7/2/17.
 */

import {addCommentByUser, addUserVoteToComment, addReplyByUser, findUserBySession} from '../models/user.model'

export const addUserComment = (req, res) => {
    if(req.query && req.query.domain){
        if(!req.query.domain.match(/www\.|http:|https:/)){
            req.query.domain = req.query.domain.replace(/\/$/,'');

            findUserBySession(req.cookies.froggo_user)
                .then(data => {
                    if(data){
                        var newComment ={
                            domain: decodeURIComponent(req.query.domain).toLocaleLowerCase(),
                            comment:req.body.comment,
                            userId: data.id
                        };

                        addCommentByUser(newComment)
                            .then(data => {
                                res.status(200).json(data);
                            }, error => {
                                res.status(500).send(error);
                            });

                    } else {
                        res.status(401).send('your session might expired')
                    }
                }, error => {
                    res.status(500).send(error);
                })
        } else {
            res.status(400).send('invalid url')
        }
    } else {
        res.status(400).send('invalid request')
    }

};

export const addUserReply = (req, res) => {
    findUserBySession(req.cookies.froggo_user)
        .then(data => {

            if(data){
                var newReply ={
                    domain: decodeURIComponent(req.query.domain).toLocaleLowerCase(),
                    reply: req.body.reply,
                    userId: data.id,
                    commentId: req.body.cid
                };

                addReplyByUser(newReply)
                    .then(data => {
                        res.status(200).json(data);
                    }, error => {
                        res.status(500).send(error);
                    });

            } else {
                res.status(401).send('your session might expired')
            }
        }, error => {
            res.status(500).send(error);
        })
};

export const addUserVote = (req, res) => {
    findUserBySession(req.cookies.froggo_user)
        .then(data => {
            if(data) {
                var newVote = {
                    id: Math.floor(Math.random() * (10000000000000 - 100 + 1)) + 100,
                    domain: decodeURIComponent(req.query.domain).toLocaleLowerCase(),
                    vote: req.body.vote,
                    userId: data.id,
                    commentId: req.body.cid
                };

                if(req.body.rid){
                    newVote.replyId = req.body.rid;
                }

                addUserVoteToComment(newVote)
                    .then(data => {
                        res.status(200).json(data);
                    }, error => {
                        res.status(500).send(error);
                    });

            } else {
                res.status(401).send('your session might expired')
            }
        }, error => {
            res.status(500).send(error);
        })
};

export const findUserBySessionId = (req, res) => {
    findUserBySession(req.cookies.froggo_user)
        .then(data => {
            if(data){
                res.status(200).json(data);
            } else {
                res.status(401).send('your session might expired')
            }
        }, error => {
            res.status(500).send(error);
        })
};

export const logout = (req, res) => {
    res.cookie("froggo_user", null, {domain: 'froggo.org', maxAge: 1});
    res.status(200).json();
};
