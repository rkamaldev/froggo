
var model = document.getElementById("model_preview");
var section = document.getElementsByClassName("section");
var node = document.getElementById('commentsList');

model.style="display:none;";


function showModel(id){
    var selectedFeed = domains.find(function(item){
        return item.id == id;
    });

    document.getElementById("model_preview").style.display="block";
    for (var i = 0;i<section.length;i++) {
        section[i].style.display = "none";
    }

    document.getElementById('preview_image').src = "./sta/images/"+selectedFeed.imageId+".jpeg";
    if(selectedFeed.domain.includes('http')){
        document.getElementById('domain').href = selectedFeed.domain;
    } else {
        document.getElementById('domain').href = 'http://www.'+selectedFeed.domain;
    }

    var comments = selectedFeed.comments;
    if(!comments.length){
        node.innerHTML = '<div class="comment_box" id="commentBox">No Comments Available</div>';
    }

    for(var i = 0; i < comments.length; i++){
        node.innerHTML += '<div class="comment_box" id="commentBox">' +
			'<img src="'+comments[i].uPhoto+'" alt="Profile" class="profile">' +
			'<div class="profile_info"><div class="title_section">' +
			'<h4 id="username">'+comments[i].name+'</h4>' +
			'<span id="vote">0 Points</span></div>' +
			'<div class="comment" id="cmnt"><p>'+comments[i].comment+'</p></div>' +
			'<div class="action"></div></div></div>';
    }
    scrollTo(0, 0);
    window.onclick = function(event){
        if(event.target == model){
            model.style.display = "none";
            for (var i = 0;i<section.length;i++) {
                section[i].style.display = "block";
            }
            node.innerHTML = "";
        }
    }
}

//Popup Post Close Button
var cls = document.getElementById("close_btn");
cls.style.cursor="pointer";
cls.onclick = function(){
    model.style.display = "none";
    node.innerHTML = "";
    for (var i = 0;i<section.length;i++) {
        section[i].style.display = "block";
    }
};

//This code is for overlay on the post when it's mobile or tablet device
function overlay(id, domain, j, cmnt, photos, users){
    var overlay = document.getElementById("overlay_"+id);
    var post_image = document.getElementById("post_image_"+id);
    overlay.classList.toggle("show_image");

    window.onclick = function(event){
        if(event.target!=post_image)
            overlay.classList.remove("show_image");
    }
}
