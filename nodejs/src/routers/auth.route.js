/**
 * Created by kamal on 7/5/17.
 */

import express from 'express';
import {handleFacebookAuth, handleAuthCallBack} from '../controllers/facebookAuth.controller'
import {handleGoogleAuth, handleGoogleAuthCallBack} from '../controllers/googleAuth.controller'
import {logout} from '../controllers/user.controller'
const Router = express.Router();

Router.get('/facebook', handleFacebookAuth);

Router.get('/facebook/callback', handleAuthCallBack);

Router.get('/google', handleGoogleAuth);
Router.get('/google/callback', handleGoogleAuthCallBack);

Router.get('/logout', logout);

export default Router;

