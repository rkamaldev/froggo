'use strict';
import {findUserBySession} from '../models/user.model';
import {findAdminBySession, findAllUsers} from '../models/admin.model';
var serverHostName = process.env.serverHostName;
module.exports.isAuthenticated = (req, res, next) => {
    if (req.cookies && req.cookies.froggo_user) {
        findUserBySession(req.cookies.froggo_user)
            .then((user) => {
                    res.cookie("froggo_user", req.cookies.froggo_user, {domain: 'froggo.org', maxAge: 365 * 24 * 60 * 60 * 1000});
                    if(req.query.domain){
                        req.query.domain = req.query.domain.replace(/\/$/,'');
                        console.log(req.query.domain)
                    }
                    return next();
                },
                () => {
                    res.sendStatus(500);
                }
            );
    } else {
        res.sendStatus(401);
    }
};

module.exports.isAdminAuthenticated = (req, res, next) => {
    if (req.cookies && req.cookies.froggo_admin_user) {
        console.log(req.cookies.froggo_admin_user)
        findAdminBySession(req.cookies.froggo_admin_user)
            .then(admin => {
                    if(admin) {
                        res.cookie("froggo_admin_user", req.cookies.froggo_admin_user, {domain: 'froggo.org', maxAge: 900000});
                        return next();
                    } else {
                        res.redirect( serverHostName + '/api/froggo/admin/login');
                    }
                },
                () =>{
                    res.sendStatus(500);
                }
            ).catch(error => console.log('Error ' + error));
    } else {
        res.redirect(serverHostName + '/api/froggo/admin/login');
    }
};