
import express from 'express';
import path from 'path';
import {isAdminAuthenticated} from './authMiddleware'
import {addManualUrl, viewNews, deleteNews, accountSetting, getAllUsers} from '../controllers/admin1.controller'
import {adminLogin, adminLogout} from '../controllers/admin.controller'
const Router = express.Router();


Router.get('/', isAdminAuthenticated, (req, res) => {res.render('views/dashboard.html', {dasgboardTitle: 'Add new url', serverHostName:process.env.serverHostName});});
Router.get('/view', isAdminAuthenticated, viewNews);
Router.get('/view/delete/:id/:imageId', isAdminAuthenticated,  deleteNews);
Router.get('/account', isAdminAuthenticated, (req, res) => {res.render('views/account.html', {errorMsg: '', dasgboardTitle: 'Account', serverHostName:process.env.serverHostName});});
Router.post('/account', isAdminAuthenticated, accountSetting);
Router.get('/users', isAdminAuthenticated, getAllUsers);
Router.post('/dashboard', isAdminAuthenticated, addManualUrl);
Router.get('/login', (req, res) => {res.render('views/index.html', {serverHostName:process.env.serverHostName});});
Router.post('/login', adminLogin);
Router.get('/logout', adminLogout);

export default Router;