/**
 * Created by kamal on 7/2/17.
 */

import express from 'express';
const Router = express.Router();
import {addUserComment, addUserVote, findUserBySessionId, addUserReply} from '../controllers/user.controller'
import {isAuthenticated} from './authMiddleware';
import validate from 'express-validation';
import schemaValidation from '../controllers/schemaValidation/schema.validation'

Router.use(isAuthenticated);
Router.post('/comments', validate(schemaValidation.domain), addUserComment);
Router.post('/reply', validate(schemaValidation.domain), addUserReply);
Router.post('/vote', validate(schemaValidation.domain), addUserVote);
Router.get('/info', findUserBySessionId);



export default Router;
