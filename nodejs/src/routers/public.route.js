/**
 * Created by kamal on 7/2/17.
 */

import express from 'express';
const Router = express.Router();
import {getCommentsByDomain, getAllDomainsData} from '../controllers/public.controller'

Router.get('/', getAllDomainsData);
Router.get('/comments', getCommentsByDomain);

export default Router;
