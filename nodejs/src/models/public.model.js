/**
 * Created by kamal on 7/2/17.
 */

import {exQuery} from '../lib/mysql';

export const getCommentsByDomainFromMongo = (domain) => {
    return exQuery('SELECT * FROM comments WHERE domain=? ORDER BY id DESC', [domain])
        .then((result) => {
            if(result && result[0] && result[0].id){
                return Promise.resolve(result);
            } else {
                return Promise.resolve([]);
            }
        }).catch((error) => {
            return Promise.reject(error);
        })
}


export const getAllDomains = () => {
    // return exQuery('SELECT * FROM domain')
    return exQuery('SELECT * FROM domain ORDER BY id DESC')
        .then(result => {
            if (result && result[0]) {

                //getting all domains to a single string
                let domains = '';
                result.map((item, index) => {
                    if(index === result.length - 1){
                        domains = domains + '"' + item.domain + '"'
                    } else {
                        domains = domains + '"' + item.domain + '",'
                    }
                    return item.domain;
                });

                let query = 'SELECT comments.userId as userId, comments.comment as comment,' +
                    ' comments.domain as domain, user.givenName as name, user.photo as uPhoto,' +
                    ' user.id as uid FROM comments LEFT JOIN user ON comments.userId=user.id' +
                    ' WHERE domain IN (' + domains + ')';

                return exQuery(query)
                    .then(result1 => {
                        if (result1 && result1[0]) {
                            //merging the comments to domains result
                            result.forEach(item => {
                                item.comments = result1.filter(comment=>{
                                    return comment.domain == item.domain;
                                });
                            })
                        }
                        return Promise.resolve(result);
                    })
                    .catch(error => {
                        return Promise.reject(error);
                    })
            } else {
                return Promise.resolve(null);
            }
        })
        .catch(error => {
            return Promise.reject(error);
        })
};
