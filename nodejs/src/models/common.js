
export const formatUrl = (url) => {
    let submit_url = url;
    let sub = null;
    let pos = null;
    let givenUrl = null;

    if (submit_url.includes('www.')) {
        sub = 'www.';
        pos = submit_url.indexOf(sub) + sub.length;
        givenUrl = submit_url.substring(pos);
    }
    else if (submit_url.includes('http://')) {
        sub = 'http://';
        pos = submit_url.indexOf(sub) + sub.length;
        givenUrl = submit_url.substring(pos);
    }
    else if (submit_url.includes('https://')) {
        sub = 'https://';
        pos = submit_url.indexOf(sub) + sub.length;
        givenUrl = submit_url.substring(pos);
    }
    else {
        sub = '';
        pos = 1;
        givenUrl = submit_url;
    }
    return givenUrl;
};


export const  mysql_real_escape_string = (str) => {
    if(!str){
        return '';
    }
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
};
