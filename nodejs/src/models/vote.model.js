/**
 * Created by kamal on 7/4/17.
 */
import {exQuery} from '../lib/mysql';

export const findVotesByCommentIds = (ids) => {
    return exQuery('SELECT * FROM vote WHERE commentId IN ('+ids.join()+')')
        .then((result) => {
            if(result && result[0] && result[0].id){
                return Promise.resolve(result);
            } else {
                return Promise.resolve([]);
            }
        }).catch((error) => {
            return Promise.reject(error);
        })
};