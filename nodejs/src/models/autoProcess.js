import {exQuery} from '../lib/mysql';
import {selenium, deleteImage, checkImageSize} from '../lib/commonFunctions';
import {formatUrl, mysql_real_escape_string} from '../models/common';
const md5 = require('md5');

export const addNewsUrlToTempTable = (valuesToInsert) => {
    return exQuery("INSERT IGNORE INTO temp_domain(domain, type, addedBy) VALUES ?", [valuesToInsert])
        .then(() => {
            return Promise.resolve('done');
        })
        .catch(error => {
            return Promise.reject(error);
        });
};

export const getLastItemFromTempTable = () => {
    return exQuery("SELECT * from temp_domain where processed=0 order by addedBy DESC limit 1")
        .then(result=>{
            if (result && result[0]) {
                return Promise.resolve(result[0]);
            } else {
                return Promise.resolve(null);
            }
        })
        .catch(error => {
            return Promise.reject(error);
        });
};

export const updateProcessedState = (id) => {
    return exQuery("UPDATE temp_domain SET processed=1 WHERE id = ?", [id])
        .then(result=>{
            return Promise.resolve(result[0]);
        })
        .catch(error => {
            return Promise.reject(error);
        });
}

export const addNewEntry = (url, categoryId, manual) => {
    const dir =  '../view/images/';
    let givenUrl = formatUrl(url);

    return exQuery("SELECT id FROM domain WHERE domain='"+mysql_real_escape_string(givenUrl)+"'")
        .then(result=>{
            if (result && result[0]) {
                return Promise.resolve('already_exist');
            } else {
                let imageId = new Date().getTime();
                return selenium.takeScreenshot(dir + imageId + ".jpeg", url, manual)
                    .then((title) => {
                        if(checkImageSize(dir + imageId + ".jpeg") < 100){
                            return Promise.reject('image not captures correctly');
                        } else {
                            return exQuery("INSERT INTO domain(domain, imageId, urlTitle, category, comment_counter) VALUES ('" + mysql_real_escape_string(givenUrl) + "', '" + imageId + "', '" + mysql_real_escape_string(title) + "', '" + categoryId + "', " + 0 + ")")
                                .then((title) => {
                                    return exQuery("SELECT id FROM domain where category = ?", [categoryId])
                                        .then(existingResult => {
                                            if (existingResult && existingResult.length > 100) {
                                                return exQuery("select * from domain order by id ASC limit 1")
                                                    .then(r => {
                                                        if(r[0]){
                                                            deleteImage(dir + r[0].imageId + ".jpeg");
                                                        }
                                                        return exQuery("delete from domain where category = ? order by id ASC limit 1", [categoryId])
                                                            .then(r => {
                                                                return Promise.resolve('done');
                                                            })
                                                            .catch(error => {
                                                                return Promise.reject(error);
                                                            });
                                                    })
                                                    .catch(error => {
                                                        return Promise.reject(error);
                                                    });

                                            } else {
                                                return Promise.resolve('done');
                                            }
                                        })
                                        .catch(error => {
                                            return Promise.reject(error);
                                        });
                                })
                                .catch(error => {
                                    return Promise.reject(error);
                                });
                        }
                    })
                    .catch(error => {
                        return Promise.reject(error);
                    });
            }
        }).catch(error => {
            return Promise.reject(error);
        });
};

export const getPageContent =(params) => {
    return selenium.getPageContent(params)
        .then((content) => {
            return Promise.resolve(content);
        })
        .catch(error => {
            return Promise.reject(error);
        });
};