import {exQuery} from '../lib/mysql';
import {deleteImage} from '../lib/commonFunctions';
import {formatUrl, mysql_real_escape_string} from './common'


const md5 = require('md5');

export const findAdminUsersByIds = (params) => {
    return exQuery('SELECT * FROM admin WHERE email =? and password =?', [params.email, md5(params.password)])
        .then(result => {
            console.dir(result)
            if(result && result[0] && result[0].aid){
                return Promise.resolve(result);
            } else {
                return Promise.resolve([]);
            }
        }).catch(error => {
            return Promise.reject(error);
        })
};

export const generateCookie1 = () => {
    let characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,'-','$','#','%'];
    let text = '';
    for( let i = 0; i < 100; i++) {
        text += characters[Math.floor(Math.random() * characters.length)];
    }
    text = text + '_' + new Date().getTime();
    return text;
};

export const updateAdminUserProfile = (cookie1) =>{
    return exQuery('UPDATE admin SET cookie=? WHERE aid=3', [cookie1])
};

export const findAdminBySession = (sessionId) => {
    return exQuery('SELECT * FROM admin WHERE cookie=?', [sessionId])
        .then(result => {
            if(result && result[0] && result[0].aid){
                return Promise.resolve(result[0]);
            } else {
                return Promise.resolve(null);
            }
        }).catch(error => {
            return Promise.reject(error);
        })
};

export const findAllUsers = () => {
    return exQuery('SELECT * FROM user')
        .then(result => {
            if(result && result[0] && result[0].id){
                return Promise.resolve(result);
            } else {
                return Promise.resolve(null);
            }
        }).catch(error => {
            return Promise.reject(error);
        })
};

export const  addManualEntry = (url, cetegoryId) => {
    let givenUrl = formatUrl(url);
    return exQuery("INSERT IGNORE INTO temp_domain(domain, type, addedBy) VALUES (?, ?, ?)", [mysql_real_escape_string(givenUrl), cetegoryId, 1])
        .then(() => {
            return Promise.resolve('done');
        })
        .catch(error => {
            return Promise.reject(error);
        });
};

export const viewAllNews = () => {
    return exQuery("SELECT * FROM domain ORDER BY id DESC")
        .then(result=>{
            if (result && result[0]) {
                return Promise.resolve(result);
            } else {
                return Promise.resolve([]);
            }
        })
        .catch(error => {
            return Promise.reject(error);
        });
};

export const deleteSelectedNews = (id, imageId) => {
    const dir =  '../view/images/';
    deleteImage(dir + imageId + ".jpeg");
    return exQuery("DELETE FROM domain WHERE id="+id)
        .catch(error => {
            return Promise.reject(error);
        });
};

export const passwordSetting = (pass, newPass, cnewPass) => {
    return exQuery("SELECT * FROM admin WHERE aid=3 and password='" + md5(pass)+"'")
        .then(result=>{
            if (result && result[0]) {
                return exQuery("UPDATE `admin` SET `password` = '"+md5(newPass)+"' WHERE `admin`.`aid` = 3")
                    .then(result=>{
                        return Promise.resolve('done');
                    })
                    .catch(error => {
                        return Promise.reject(error);
                    });
            } else {
                return Promise.resolve('current password not correct');
            }
        })
        .catch(error => {
            return Promise.reject(error);
        });
};
