/**
 * Created by kamal on 7/2/17.
 */
import {exQuery} from '../lib/mysql'

export const addCommentByUser = (newComment) => {
    console.dir(newComment)
    return exQuery('INSERT INTO comments SET ?',  newComment)
};

export const addReplyByUser = (newReply) => {
    return exQuery('INSERT INTO reply SET ?',  newReply)
};

export const addUserVoteToComment = (newVote) => {
    if(!newVote.replyId) {
        newVote.replyId = -1;
    }
    return exQuery('INSERT INTO vote SET ? ON DUPLICATE KEY UPDATE ' + 'vote=VALUES(vote)', newVote)
};

export const findUsersByIds = (ids) => {
    ids = ids.filter((id) => {
        return id != null;
    });

    return exQuery('SELECT * FROM user WHERE id IN (' + ids.join()+')')
        .then((result)=>{
            if(result && result[0] && result[0].id){
                return Promise.resolve(result);
            } else {
                return Promise.resolve([]);
            }
        }).catch((error) => {
            return Promise.reject(error);
        })
};

export const findUserBySession = (sessionId) => {
    console.log(sessionId)
    return exQuery('SELECT * FROM user WHERE session=?', [sessionId])
        .then((result) => {
            if(result && result[0] && result[0].id){
                return Promise.resolve(result[0]);
            } else {
                console.log(result)
                return Promise.resolve(null);
            }
        }).catch((error) => {
            console.log(error)
            return Promise.reject(error);
        })
};

export const generateCookie = () => {
    let characters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,'-','$','#','%'];
    let text = '';
    for( let i = 0; i < 100; i++) {
        text += characters[Math.floor(Math.random() * characters.length)];
    }
    text = text + '_' + new Date().getTime();
    return text;
};

export const updateUserPofile = (user) => {
    return exQuery('INSERT INTO user SET ? ON DUPLICATE KEY UPDATE ' +
        'name=VALUES(name),familyName=VALUES(familyName),photo=VALUES(photo),' +
        'lastLogin=VALUES(lastLogin),session=VALUES(session),givenName=VALUES(givenName)', user)
};

