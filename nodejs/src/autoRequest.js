/**
 * Created by kamal on 10/2/17.
 */
const fs = require('fs');
const request = require('request');
const async = require("async");

let extractedItem = [];
let extractedItemBBC = [];
let extractedItemHUF = [];

const newsType = {
    1: 'Politics',
    2: 'Business',
    3: 'Technology',
    4: 'Entertainment',
    5: 'Sport'
};

process.env.newsType = newsType;

import {
    addNewsUrlToTempTable,
    getLastItemFromTempTable,
    updateProcessedState,
    addNewEntry,
    getPageContent
} from './models/autoProcess';

import {
    formatUrl,
    mysql_real_escape_string
} from './models/common';

function startGetUrls(){
    console.log('started');
    extractedItem = [];
    extractedItemBBC = [];
    extractedItemHUF = [];

    async.eachSeries([
           // {id: 1, type:1, url:'http://www.bbc.com/news/politics/', match: /href="\/news\/[^"]+" class="title-link"/g},
           // {id: 1, type:2, url:'http://www.bbc.com/news/business', match: /href="\/news\/[^"]+" class="title-link"/g},
           //  {id: 1, type:3, url:'http://www.bbc.com/news/technology', match: /href="\/news\/[^"]+" class="title-link"/g},
           //  {id: 1, type:4, url:'http://www.bbc.com/news/entertainment_and_arts', match: /href="\/news\/[^"]+" class="title-link"/g},
           //  {id: 1, type:5, url:'http://www.bbc.com/sport', match: /<a href="\/sport\/[^"]+/g},
           // //  {id: 2, type:1, url:'http://www.nydailynews.com/news/politics', match: /href="http:\/\/www.nydailynews.com\/news\/[^"]+/g},
           // //  {id: 2, type:4, url:'http://www.nydailynews.com/news/politics', match: /href="http:\/\/www.nydailynews.com\/entertainment\/[^"]+/g},
           // // {id: 2, type:5, url:'http://www.nydailynews.com/news/politics', match: /href="http:\/\/www.nydailynews.com\/sports\/[^"]+/g},
           //  {id: 3, type:2, url:'http://www.businessinsider.com/', match: /<a class="title" href="http:\/\/www.businessinsider.com\/[^"]+/g},
           //  {id: 4, type:3, url:'https://www.theguardian.com/uk/technology', match: /href="https:\/\/www.theguardian.com\/technology\/[^"]+/g},
           //  {id: 5, type:2, url:'https://www.usnews.com/news/business', match: /href="https:\/\/www.usnews.com\/news\/business\/articles\/[^"]+/g},
            {id: 6, type:3, url:'https://techcrunch.com/', match: /href="https:\/\/techcrunch.com\/2017\/[^"]+/g},

        ],
        function(item, callback){
            getPageContent(item)
                .then(content => {
                    let valuesToInsert = [];
                    if(item.id === 1){
                        let matches = content.match(item.match);
                        if(matches) {
                            for (let i = 0; i < matches.length; i++) {
                                let tempUrl = matches[i].match(/href="[^"]+/)[0].replace('href="', '');
                                if(!tempUrl.match('www.bbc.')){
                                    tempUrl = 'http://www.bbc.com' + tempUrl;
                                }
                                if(tempUrl.length > 30 && !tempUrl.match('https') ){
                                    valuesToInsert.push([
                                        mysql_real_escape_string(formatUrl(tempUrl)),
                                        item.type,
                                        0
                                    ]);
                                }
                            }
                        }
                    } else if(item.id === 2){
                        let matches = content.match(item.match);
                        if(matches) {
                            for (let i = 0; i < matches.length; i++) {
                                let tempUrl = matches[i].match(/href="[^"]+/)[0].replace('href="', '');
                                if(tempUrl.length > 30 && !tempUrl.match('https') ){
                                    valuesToInsert.push([
                                        mysql_real_escape_string(formatUrl(tempUrl)),
                                        item.type,
                                        0
                                    ]);
                                }
                            }
                        }
                    }
                    else if(item.id === 3){
                        let matches = content.match(item.match);
                        if(matches) {
                            for (let i = 0; i < matches.length; i++) {
                                let tempUrl = matches[i].match(/href="[^"]+/)[0].replace('href="', '');
                                if(tempUrl.length > 30 && !tempUrl.match('https') ){
                                    valuesToInsert.push([
                                        mysql_real_escape_string(formatUrl(tempUrl)),
                                        item.type,
                                        0
                                    ]);
                                }
                            }
                        }
                    }
                    else if(item.id === 4){
                        let matches = content.match(item.match);
                        if(matches) {
                            for (let i = 0; i < matches.length; i++) {
                                let tempUrl = matches[i].match(/href="[^"]+/)[0].replace('href="', '');
                                if(tempUrl.length > 30){
                                    valuesToInsert.push([
                                        mysql_real_escape_string(formatUrl(tempUrl)),
                                        item.type,
                                        0
                                    ]);
                                }
                            }
                        }
                    } else {
                        let matches = content.match(item.match);
                        console.dir(matches)
                        if(matches) {
                            for (let i = 0; i < matches.length; i++) {
                                let tempUrl = matches[i].match(/href="[^"]+/)[0].replace('href="', '');
                                if(tempUrl.length > 30){
                                    valuesToInsert.push([
                                        mysql_real_escape_string(formatUrl(tempUrl)),
                                        item.type,
                                        0
                                    ]);
                                }
                            }
                        }
                    }


                    if(valuesToInsert.length){
                        addNewsUrlToTempTable(valuesToInsert)
                            .then(()=>{
                                callback();
                            })
                            .catch(error=> {
                                callback();
                            });
                    } else {
                        callback();
                    }
                })
                .catch(error=> {
                    callback();
                    console.log(error)
                });
        },
        (err) => {
            console.log('all done');
            makePic();
        }
    );
}


function makePic(){
    getLastItemFromTempTable()
        .then((result) => {
            if(result){
                addNewEntry('http://www.' + result.domain, result.type + '')
                    .then((response)=>{
                        updateProcessedState(result.id)
                            .then(()=>{
                                makePic()
                            })
                            .catch((error)=>{
                                console.log(error);
                            });

                    })
                    .catch((error)=>{
                        console.log(error);
                        makePic();
                    })
            }
        })
}

function startAutoRequest(){
    getLastItemFromTempTable()
        .then((result) => {
            if (result) {
                makePic();
            } else {
                startGetUrls();
            }
        });

}

startAutoRequest();
setInterval(()=>{
    startAutoRequest();
}, (1000 * 60 * 60));

