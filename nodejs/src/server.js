/**
 * Created by kamal on 7/1/17.
 */

'use strict';
import path from 'path';
import compression from 'compression';
import express from 'express';
import cookieParser from 'cookie-parser';

const port = (process.env.PORT || 3000);

import publicRouter from './routers/public.route';
import userRouter from './routers/user.route';
import authRouter from './routers/auth.route'
import adminRouter from './routers/admin.route'

import passport from './lib/facebookAuth';

const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use((err, req, res, next) => {res.status(400).json(err)});
app.use(compression());

app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());

app.set('views', 'src/view');
app.engine('html', require('ejs').renderFile);

app.use('/api/froggo/public', publicRouter);
app.use('/api/froggo/user', userRouter);
app.use('/api/froggo/auth', authRouter);
app.use('/api/froggo/admin', adminRouter);

app.use('/froggo/assets', express.static(path.join(__dirname, './view/')));

const listener = app.listen(port, (err) => {
    if (err) {
        console.error(err);
        return;
    }
    console.log(`Application is listening on port ${listener.address().port}`);
});

export default app;