const mysql = require('mysql');
const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "froggo"
});

let mysqlConnection = null;

function connect() {
    return new Promise((resolve, reject) => {
        if (mysqlConnection) {
            resolve(mysqlConnection);
        } else {
            con.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
                mysqlConnection = con;
                resolve(mysqlConnection);

            });
        }
    })
}

var instance = {
    exQuery: (query, params) => {
        return connect()
            .then(() => {
                return new Promise((resolve, reject) => {
                    mysqlConnection.query(query, params, (err, rows, fields) => {
                        if (err) {
                            if(err.code === 'PROTOCOL_CONNECTION_LOST'){
                                return connect()
                                    .then(() => {
                                       return instance.exQuery(query, params);
                                    })
                            }else{
                                reject(err);
                            }
                        } else {
                            resolve(rows);
                        }
                    });
                });
            })
    }
};

module.exports = instance;