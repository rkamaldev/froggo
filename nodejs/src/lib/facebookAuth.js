/**
 * Created by kamal on 7/5/17.
 */

const authConfig = require('./authConfig');

const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy;
const serverHostName = process.env.serverHostName;

passport.use(new FacebookStrategy({
        clientID: authConfig.facebook.FACEBOOK_APP_ID,
        clientSecret: authConfig.facebook.FACEBOOK_APP_SECRET,
        callbackURL: serverHostName + authConfig.facebook.CALLBACK_URL,
        profileFields: ['id', 'displayName', 'name', 'photos']
    },
    (accessToken, refreshToken, profile, done) => {
        let userData = {
            profile: profile,
            accessToken: accessToken
        };

        done(null, userData);
    }
));

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});

module.exports = passport;