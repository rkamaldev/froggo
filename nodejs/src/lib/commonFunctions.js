const path = require('path');
const fs = require('fs');
const cmd=require('node-cmd');
const seleniumUrl = 'http://127.0.0.1:4444/wd/hub';
const dockerName = 'selenium';

const selenium = {
    takeScreenshot:(dir, url, manual) => {
        return new Promise((resolve, reject)=>{
            let timeout = null;
            cmd.get(
                'docker rmi $(docker images -f "dangling=true" -q); sudo docker restart ' + dockerName + ';',(err, data, stderr) => {
                    const {Builder, By, Key, until} = require('selenium-webdriver');
                    let driver = new Builder()
                        .withCapabilities({
                            browserName: 'chrome',
                            javascriptEnabled: true,
                            acceptInsecureCerts: true,
                            chromeOptions: {
                                'args': ['--disable-web-security']
                            }
                        })
                        .usingServer(seleniumUrl)
                        .build();
                    // timeout = setTimeout(()=> {
                    //     driver.quit();
                    //     reject();
                    // }, 30000);
                    driver.get(url);
                    driver.executeScript('console.log("ssssss")');
                    driver.manage().window().setSize(1024, 2000);
                    driver.getTitle().then(title => {
                        console.log(title);
                        driver.takeScreenshot().then(
                            (image, err) => {
                                if (err) {
                                    clearTimeout(timeout);
                                    reject();
                                }
                                fs.writeFile(path.join(__dirname, dir), image, 'base64', (err1)=> {
                                    if (err1) {
                                        clearTimeout(timeout);
                                        reject();
                                    } else {
                                        clearTimeout(timeout);
                                        resolve(title);
                                    }
                                });
                            }
                        ).catch(error => {
                            clearTimeout(timeout);
                            reject(error);
                        })
                    }).catch(error => {
                        clearTimeout(timeout);
                        reject(error);
                    })
                });
        })
    },
    getPageContent(params){
        return new Promise((resolve, reject)=>{
            cmd.get(
                'sudo docker rmi $(docker images -f "dangling=true" -q); sudo docker restart ' + dockerName + ';',(err, data, stderr) => {
                    let timeout = null;
                    const {Builder, By, Key, until} = require('selenium-webdriver');
                    let driver = new Builder()
                        .withCapabilities({
                            browserName: 'chrome',
                            javascriptEnabled: true,
                            acceptInsecureCerts: true,
                            chromeOptions: {
                                'args': ['--disable-web-security']
                            }
                        })
                        .usingServer(seleniumUrl)
                        .build();
                    // timeout = setTimeout(()=> {
                    //     driver.quit();
                    //     reject();
                    // }, 30000);
                    driver.get(params.url);
                    driver.executeScript('console.log("ssssss")');
                    driver.getPageSource()
                        .then((content, error) => {
                            if (error) {
                                clearTimeout(timeout);
                                reject(error);
                            } else {
                                clearTimeout(timeout);
                                resolve(content)
                            }
                        })
                        .catch(error => {
                            clearTimeout(timeout);
                            reject(error);
                        })
                })
        })
    }
};

const deleteImage = (dir)=>{
    return new Promise((resolve, reject) => {
        fs.unlinkSync(path.join(__dirname, dir));
        resolve();
    });
};

const checkImageSize = (dir)=> {
    let stats = fs.statSync(path.join(__dirname, dir))
    if(stats.size){
        return stats.size
    } else {
        return null;
    }
}


module.exports.selenium = selenium;
module.exports.deleteImage = deleteImage;
module.exports.checkImageSize = checkImageSize;
