const authConfig = require('./authConfig');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuthStrategy;
const serverHostName = process.env.serverHostName;

passport.use(new GoogleStrategy({
        consumerKey: authConfig.google.GOOGLE_APP_ID,
        consumerSecret: authConfig.google.GOOGLE_APP_SECRET,
        callbackURL: serverHostName + authConfig.google.CALLBACK_URL
    },
    function(token, tokenSecret, profile, done) {
        User.findOrCreate({ googleId: profile.id }, (err, user) => {
            return done(err, user);
        });
    }
));

module.exports = passport;
